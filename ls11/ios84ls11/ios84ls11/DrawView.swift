//
//  DrawView.swift
//  ios84ls11
//
//  Created by WA on 04.03.2021.
//

import UIKit

class DrawView: UIView {

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 20, y: 60))
        path.addLine(to: CGPoint(x: 20, y: 160))
        path.addLine(to: CGPoint(x: 120, y: 160))
        path.addLine(to: CGPoint(x: 120, y: 60))
        path.addLine(to: CGPoint(x: 20, y: 60))

        path.close()

        path.lineWidth = 10

        UIColor.red.setStroke()
        UIColor.blue.setFill()

        path.fill()
        path.stroke()
    }
}
