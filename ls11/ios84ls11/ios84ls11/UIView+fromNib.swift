//
//  UIView+fromNib.swift
//  ios84ls11
//
//  Created by WA on 04.03.2021.
//

import UIKit

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle(for: T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
