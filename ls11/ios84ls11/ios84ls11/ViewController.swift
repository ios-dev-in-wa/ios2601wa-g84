//
//  ViewController.swift
//  ios84ls11
//
//  Created by WA on 04.03.2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // DRAWING
//        let drawView = DrawView(frame: CGRect(x: 0, y: 0, width: 375, height: 500))
//
//        view.addSubview(drawView)
        print(Settings.shared.isAudioEnabled, Settings.shared.userName)
        Settings.shared.isAudioEnabled = true
        Settings.shared.userName = "Artwwwem"
        
    }

    @IBAction func buttonAction(_ sender: Any) {
        guard let customAlertVC = storyboard?.instantiateViewController(identifier: "CustomAlertVC") as? CustomAlertViewController else { return }
        customAlertVC.modalPresentationStyle = .overCurrentContext
        customAlertVC.setupAlert(model: AlertModel(isPositive: true, title: "YOU ARE WINNER!", subtitle: "YOU WIN 100 BUCKS!!"))
        present(customAlertVC, animated: true, completion: nil)
    }
}

//extension ViewController: CustomAlertViewDelegate {
//    func buttonAction() {
//        print("ALERT BUTTON PRESSED")
//    }
//}
