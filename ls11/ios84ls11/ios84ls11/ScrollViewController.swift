//
//  ScrollViewController.swift
//  ios84ls11
//
//  Created by WA on 04.03.2021.
//

import UIKit
import Kingfisher

class ScrollViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.kf.setImage(with: URL(string: "https://cms-assets.tutsplus.com/uploads/users/107/posts/26488/final_image/41-space-scrolling-background850-2.jpg"))
    }
    
}
