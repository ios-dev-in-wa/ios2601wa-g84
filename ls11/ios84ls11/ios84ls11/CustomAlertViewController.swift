//
//  CustomAlertViewController.swift
//  ios84ls11
//
//  Created by WA on 04.03.2021.
//

import UIKit

class CustomAlertViewController: UIViewController {

    var customAlertView: CustomAlertView?

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
//        setupAlert()
        // Do any additional setup after loading the view.
    }

    func setupAlert(model: AlertModel) {
        let customView: CustomAlertView = .fromNib()
        customAlertView = customView
//        customView.frame = CGRect(x: 10, y: 10, width: 400, height: 500)
//        customView.backgroundColor = .cyan
        customView.setupWith(model: model)
//        customView.delegate = self
        
        customView.buttonAction = { [weak self] in
            print("BUTTON ACTION FROM ALERT")
            self?.dismiss(animated: true, completion: nil)
        }
        view.addSubview(customView)
        customView.center = view.center
    }
}
