//
//  Settings.swift
//  ios84ls11
//
//  Created by WA on 04.03.2021.
//

import Foundation

class Settings {

    static let shared = Settings()
    private var some = 2

    private init() {}

    private func doo() { }
    private let defaults = UserDefaults.standard

    var isAudioEnabled: Bool {
        get {
            return defaults.bool(forKey: "isAudioEnabled")
        }
        set {
            defaults.set(newValue, forKey: "isAudioEnabled")
        }
    }

    var audioLevel: Double {
        get {
            return defaults.double(forKey: "audioLevel")
        }
        set {
            defaults.set(newValue, forKey: "audioLevel")
        }
    }

    var userName: String {
        get {
            return defaults.string(forKey: "userName") ?? "Username is not set"
        }
        set {
            defaults.set(newValue, forKey: "userName")
        }
    }
}
