//
//  CustomAlertView.swift
//  ios84ls11
//
//  Created by WA on 04.03.2021.
//

import UIKit

struct AlertModel {
    let isPositive: Bool
    let title: String
    let subtitle: String
}

//protocol CustomAlertViewDelegate: class {
//    func buttonAction()
//}

class CustomAlertView: UIView {
    @IBOutlet weak var statusImageView: UIImageView!
    @IBOutlet weak var circleContainerView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!

    var buttonAction: (() -> Void)?
//    weak var delegate: CustomAlertViewDelegate?

    func setupWith(model: AlertModel) {
        statusImageView.image = model.isPositive ? UIImage(systemName: "checkmark") : UIImage(systemName: "xmark")
        circleContainerView.backgroundColor = model.isPositive ? .green : .red
        actionButton.backgroundColor = model.isPositive ? .green : .red

        titleLabel.text = model.title
        subtitleLabel.text = model.subtitle
    }

    @IBAction func buttonAction(_ sender: UIButton) {
//        delegate?.buttonAction()
        buttonAction?()
    }
}
