//
//  AppCoordinator.swift
//  ios84ls16
//
//  Created by WA on 23.03.2021.
//

import UIKit

class AppCoordinator {

    func showMainScreen(window: UIWindow) {
        let userLoggedIn = true//PFUser.current() != nil

        if userLoggedIn {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateInitialViewController()
            window.rootViewController = vc
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MainScreenId")
            window.rootViewController = vc
        }
        window.makeKeyAndVisible()
    }
}
