//
//  ColorCollectionViewCell.swift
//  ios84ls16
//
//  Created by WA on 23.03.2021.
//

import UIKit

class ColorCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var valueLabel: UILabel!

    func setShadow() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 20, height: 10.0)
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 0.5
        layer.masksToBounds = false
    }
}
