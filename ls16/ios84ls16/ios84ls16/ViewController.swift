//
//  ViewController.swift
//  ios84ls16
//
//  Created by WA on 23.03.2021.
//

import UIKit
//"blackColorKey" = "black";
//"redColorKey" = "red";
//"blueColorKey" = "blue";
//"greenColorKey" = "green";
//"yellowColorKey" = "yellow";

enum Color: String, CaseIterable {
    case black = "blackColorKey"
    case blue = "blueColorKey"
    case red = "redColorKey"
    case green = "greenColorKey"
    case yellow = "yellowColorKey"

    var value: UIColor {
        switch self {
        case .black: return .black
        case .blue: return .blue
        case .green: return .green
        case .red: return .red
        case .yellow: return .yellow
        }
    }
}

class ViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!

    var colors = Color.allCases

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        print(Settings.shared.sliderValue)
        print(Settings.shared.audioSetting)
    }

    @IBAction func doneAction(_ sender: UIBarButtonItem) {
        Settings.shared.sliderValue = 1
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if let url = URL(string: "tg://+380222222") {
            UIApplication.shared.open(url)
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
    }
}

extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorCollectionViewCell", for: indexPath) as? ColorCollectionViewCell else {
            fatalError()
        }
        print(indexPath.item, indexPath.row)
        let color = colors[indexPath.row]
        cell.valueLabel.text = color.rawValue.localized.capitalized
        cell.valueLabel.textColor = color.value == .black ? .white : .black
        cell.backgroundColor = color.value
        cell.setShadow()
        return cell
    }
}

extension ViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let alert = UIAlertController(title: "errorTitleKey".localized, message: "tryAgainKey".localized, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
