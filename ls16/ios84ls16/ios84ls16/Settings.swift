//
//  Settings.swift
//  ios84ls16
//
//  Created by WA on 23.03.2021.
//

import Foundation

class Settings {
    static let shared = Settings()

    private init() { }

    private let userDefaults = UserDefaults.standard

    var sliderValue: Double {
        get {
            return userDefaults.double(forKey: "slider_preference")
        }
        set {
            userDefaults.setValue(newValue, forKey: "slider_preference")
        }
    }

    var audioSetting: String {
        get {
            return userDefaults.string(forKey: "audioSettingKey") ?? "-"
        }
        set {
            userDefaults.setValue(newValue, forKey: "audioSettingKey")
        }
    }
}
