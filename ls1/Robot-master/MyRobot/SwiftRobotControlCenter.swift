//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//
import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    //  Level name setup
    override func viewDidLoad() {
        levelName = "L3C" //  Level name
        super.viewDidLoad()
    }
    
    override func run() {
        //        forExample()
//        whileExample()
//        foo()
        findPeak()
        goToPeak()
        uTurn()
        moveDown()
    }
    
    func findPeak() {
        
    }

    func goToPeak() {
        
    }

    func uTurn() {
        
    }

    func moveDown() {
        
    }
    
    /*
     func forExample() {
     for _ in 0...30 {
     move()
     }
     turnRight()
     }
     */
    
    func foo() {
        move()
    }

    /// Пример использования цикла while
    func whileExample() {
        //        while true {
        //            move()
        //            if candyPresent {
        //                break
        //            }
        //        }
        foo()
        while !candyInBag {
            move()
            if candyPresent {
                break
                //                pick()
                //                turnLeft()
            }
        }
    }
    
    // ТАК НЕ ДЕЛАТЬ
    //    func snake_case() {
    //
    //    }
    
    func firstPuzzleSolved() {
        doubleMove()
        doubleMove()
        doubleMove()
        move()
        pick()
        doubleMove()
        turnRight()
        move()
        put()
        turnLeft()
        doubleMove()
    }
    
    func doubleMove() {
        move()
        move()
    }
    
    func turnLeft() {
        turnRight()
        turnRight()
        turnRight()
    }
    
    func ifElseExample() {
        if frontIsClear {
            doubleMove()
            move()
            if candyPresent {
                pick()
                turnRight()
                doubleMove()
            } else {
                move()
                pick()
            }
        }
        
    }
}
