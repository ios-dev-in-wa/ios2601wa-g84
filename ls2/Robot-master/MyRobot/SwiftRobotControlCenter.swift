//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//
import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    //  Level name setup
    override func viewDidLoad() {
        levelName = "L4H" //  Level name
        super.viewDidLoad()
    }
    
    // MARK: - 222
    override func run() {
        //        while rightIsClear {
        //            crossLeft()
        //        }
        //        turnLeft()
        //        while frontIsClear {
        //            move()
        //        }
        //        turnLeft()
        //        while leftIsClear {
        //            crossRight()
        //            put()
        //        }
        makeX()
    }
    
    func makeX() {
        while frontIsClear {
            put()
            move()
            turnRight()
            if frontIsBlocked { break }
            move()
            turnLeft()
        }
        
        while !facingUp {
            turnLeft()
        }
        
        while frontIsClear {
            move()
        }
        turnLeft()
        move()
        
        while frontIsClear {
            put()
            move()
            turnLeft()
            move()
            turnRight()
        }
        put()
    }
    
    func turnLeft() {
        turnRight()
        turnRight()
        turnRight()
    }
    func crossLeft() {
        put()
        move()
        turnRight()
        move()
        put()
        turnLeft()
    }
    func crossRight() {
        put()
        turnLeft()
        move()
        turnRight()
        move()
    }
    
}
