//
//  ViewController.swift
//  iosg84ls2
//
//  Created by WA on 28.01.2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        greeting()
//        greetingFor(name: "Armen")
//        greetingFor(name: "Artur")
//        greetingFor(name: "Artem")
//        greetingFor(name: "Dima")
//        basicTypes()
        let greetingForArtem = makeGreetingText(name: "Artem", daytime: 12)
        let greetingForDima = makeGreetingText(name: "Dima", daytime: 8)
        print(greetingForArtem)
        print(greetingForDima)
        math()
        switchExample(name: "Artem", intValue: 10)
        switchExample(name: "1111", intValue: 0)
        viewExample(x: 0, color: .red, author: "Artem")
        viewExample(x: 50, color: .black, author: "Artur")
        viewExample(x: 350, color: .cyan, author: "")
        print(isNumberValid(number: -1))
        print(isNumberValid(number: 100))
        greetings(times: 10)
    }

    func greeting() {
        print("Hello")
        print("NICKNAME")
    }

    func isNumberValid(number: Int) -> Bool {
        if number >= 0 {
            return false
        } else {
            return true
        }
    }

    func greetingFor(name: String) {
        print("Hello", name)
    }

    func basicTypes() {
        let userName = "Artem"
        let userNameTwo: String = "Dmitriy"

        let intValue = 10
        var result = intValue * 20
        result = 1000
        print(userName, userNameTwo, result)
        
        // String, Int, Double, Bool
        let isTheWeatherGood = true
        greeting(isTheWeatherGood: isTheWeatherGood)

        let doubleValue: Int = Int(2.5)
        print(doubleValue)

        let realDouble = 3.14
        if realDouble.isFinite {
            print(realDouble, "is FINITE")
        }

        let stringName = "Artem"
        print(stringName.lowercased())

        print(String(realDouble))

//        let some = "2020" as? Int
    }

    func greeting(isTheWeatherGood: Bool) {
        if isTheWeatherGood {
            print("Have a nice day!")
        } else {
            print("Sorry for that!")
        }
    }

    func makeGreetingText(name: String, daytime: Int) -> String {
        return "Hello, \(name), time is: \(daytime)"
    }

    func math() {
        let result = 2 + 2
        let resultTwo = result - 2 * 5 / 3

        let ostatok = 7 % 2
        print(ostatok)

        let mathExample = 10 > 5 // == > < >= <= !=
        print(mathExample)

        let some = 20 * 3
        let someTwo = 20 / 3
        if some != someTwo {
            print("BOOM")
        }

        print(Double.pi)
    }

    func greetings(times: Int) {
        for _ in 0..<times {
            print("GREETING")
        }
    }
    func switchExample(name: String, intValue: Int) {
        switch name {
        case "Artem":
            print("hello there")
        case "Dima":
            print("Bye bye")
        default: print("NOT EXPECTED")
        }
        switch intValue {
        case 0:
            print("IT's ZERO")
        case 1...20:
            print("YOU WIN")
        default: print("YOU LOOSE")
        }
    }

    func viewExample(x: CGFloat, color: UIColor, author: String) {
        let boxView = UIView()
        boxView.frame = CGRect(x: x, y: 20, width: 100, height: 200)
        boxView.backgroundColor = color

        view.addSubview(boxView)
        print(author)
    }
}

