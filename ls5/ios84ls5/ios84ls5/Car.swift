//
//  Car.swift
//  ios84ls5
//
//  Created by WA on 09.02.2021.
//

import UIKit

enum BodyType {
    case sedan, coope, universal
}

class Car {
    let bodyType: BodyType
    var color: UIColor
    var zagreznenie: Double = 0

    let name = "CAR"

    init(bodyType: BodyType, color: UIColor) {
        self.bodyType = bodyType
        self.color = color
    }

    func startEngine() {
        
    }

    func stopEngine() {
        
    }

    func ride() {
        print("car drives front")
    }
}
