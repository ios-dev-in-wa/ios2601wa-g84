//
//  BattleCar.swift
//  ios84ls5
//
//  Created by WA on 09.02.2021.
//

import Foundation

enum GunType {
    case minigun, bazooka
}

class BattleCar: Car {
    let gunType: GunType

    init(gunType: GunType, bodyType: BodyType) {
        self.gunType = gunType
        super.init(bodyType: bodyType, color: .gray)
    }

    // меняет цвет машинки
    func redraw() {
        color = .green
    }

    override func ride() {
        print("BUGAGAGA")
//        super.ride()
    }
}

class TankCar: BattleCar {
    override func ride() {
        super.ride()
    }

    override func redraw() {
        
    }
}
