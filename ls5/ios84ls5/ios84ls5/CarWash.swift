//
//  File.swift
//  ios84ls5
//
//  Created by WA on 09.02.2021.
//

import Foundation

enum Drink: String {
    case espresso, latte

    var waterNeed: Double {
        switch self {
        case .espresso: return 10.0
        case .latte: return 20.0
        }
    }

    var coffeeNeed: Double {
        switch self {
        case .espresso: return 20.0
        case .latte: return 30.0
        }
    }
}

class CarWash {
    var soapTank = 100.0

    var ochered: [Car] = []

    func prepare() -> Car {
        if let car = ochered.first {
            car.startEngine()
            car.ride()
            car.stopEngine()
            ochered.removeFirst()
            return car
        } else {
            fatalError()
        }
        make(drink: .espresso)
    }

    func make(drink: Drink) {
//        if drink.waterNeed
//        if drink.coffeeNeed
    }

    func automateQueue() {
        for car in ochered {
            clean(car: prepare())
        }
    }

    func clean(car: Car) {
        if soapTank >= 10.0 {
            print("Car was dirty - ", car.zagreznenie)
            soapTank -= 10.0
            car.zagreznenie = 0
            print("Car is clean now")
        } else {
            print("Add soap to the car wash")
        }
    }
}
