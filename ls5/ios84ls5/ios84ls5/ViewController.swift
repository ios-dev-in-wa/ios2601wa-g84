//
//  ViewController.swift
//  ios84ls5
//
//  Created by WA on 09.02.2021.
//

import UIKit

class ViewController: UIViewController {

    let blackCar = Car(bodyType: .sedan, color: .black)
    let blackBattleCar = BattleCar(gunType: .bazooka, bodyType: .sedan)
    let tankCar = TankCar(gunType: .minigun, bodyType: .universal)

    let carWash = CarWash()

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var boxView: UIView!
    @IBOutlet weak var pressMeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // PARENT
//        blackCar.bodyType
//        blackCar.color
        blackCar.ride()
        

        // SUBCLASS
//        blackBattleCar.bodyType
//        blackBattleCar.color
        blackBattleCar.ride()
        // NEW
        blackBattleCar.gunType
        blackBattleCar.redraw()

        let arrayOfCars = [blackCar, blackBattleCar, tankCar]
        
        

        for car in arrayOfCars {
            car.zagreznenie = Double.random(in: 0...1)
            carWash.clean(car: car)
        }

//        view
        boxView.backgroundColor = .gray
    }

    func doSomth() {
        boxView.backgroundColor = .magenta
    }

    @IBAction func pressMeButtonAction(_ sender: UIButton) {
//        sender.setTitle("bla", for: .default)
        sender.backgroundColor = .cyan
        boxView.frame.origin = view.center
        statusLabel.text = "Button was pressed"
        doSomth()
        carWash.prepare()
    }
    
}

