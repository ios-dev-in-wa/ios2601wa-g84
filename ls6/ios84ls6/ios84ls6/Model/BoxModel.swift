//
//  BoxModel.swift
//  ios84ls6
//
//  Created by WA on 11.02.2021.
//

import Foundation

class BoxModel {

    func numberOfBoxes() -> Int {
        return Int.random(in: 0...5)
    }
}
