//
//  RedView.swift
//  ios84ls6
//
//  Created by WA on 11.02.2021.
//

import UIKit

class RedView: UIView {

    init() {
        super.init(frame: .zero)
        backgroundColor = .red
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
