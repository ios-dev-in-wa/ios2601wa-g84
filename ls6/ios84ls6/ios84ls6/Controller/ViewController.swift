//
//  ViewController.swift
//  ios84ls6
//
//  Created by WA on 11.02.2021.
//

import UIKit

class ViewController: UIViewController {

    let model = BoxModel()
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let redView = RedView()
//        redView.frame = CGRect(x: 20, y: 20, width: 100, height: 100)
//
//        view.addSubview(redView)
        
        // Do any additional setup after loading the view.
        move()
    }
    
    func move() {
        //
        print("MoveFunc")
    }

    func addRedBoxes() {
        for i in 0...model.numberOfBoxes() {
            let redViewTwo = RedView()
            redViewTwo.frame = CGRect(x: 40 * CGFloat(i), y: 20, width: 30, height: 30)
            view.addSubview(redViewTwo)
        }
    }

    @IBAction func midButtonAction(_ sender: UIButton) {
        addRedBoxes()
        if let name = nameTextField.text {
            nameLabel.text = " Hello, \(name)"
        }
    }

    @IBAction func nameEdittingDidEndAction(_ sender: UITextField) {
        sender.backgroundColor = .clear
    }

    @IBAction func nameEdittingDidBeginAction(_ sender: UITextField) {
        sender.backgroundColor = .blue
    }
}

