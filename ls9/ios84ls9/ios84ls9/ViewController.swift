//
//  ViewController.swift
//  ios84ls9
//
//  Created by WA on 25.02.2021.
//

import UIKit

class Student {
    let name: String
    let slackName: String

    init(name: String, slackName: String) {
        self.name = name
        self.slackName = slackName
    }
}

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var students = [
        Student(name: "Artem", slackName: "@bazinga"),
        Student(name: "Artem", slackName: "@bazinga"),
    ]
    var names: [String] = ["Artem", "Ilona", "Anton"]
    // COMPUTED PROPERTY
//    var computedProperty: [String] {
//        names.filter { string -> Bool in
//            string.first == "A"
//        }
//    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // MUST HAVE
        tableView.dataSource = self
        tableView.delegate = self
        // REMOVE EMPTY CELLS ON BOTTOM
        tableView.tableFooterView = UIView()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoToDetails" {
            if let indexPath = tableView.indexPathForSelectedRow ?? sender as? IndexPath {
                if let detailsVC = segue.destination as? DetailsViewController {
                    detailsVC.userName = names[indexPath.row]
                } else {
                    print("SOMETHING WENT WRONG DESTINATION IS NOT DETAILSVC")
                }
            }
        }
    }

    func removeDataFor(indexPath: IndexPath) {
        names.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
    }

    func copyDataFor(indexPath: IndexPath) {
        let value = names[indexPath.row]
        names.insert(value, at: indexPath.row)
        tableView.insertRows(at: [indexPath], with: .automatic)
    }

    @IBAction func editButtonAction(_ sender: UIBarButtonItem) {
//        names.append("Avram")
        // HARD RESET
//        tableView.reloadData()
        // USER FRIENDLY RELOAD
//        tableView.reloadSections([0], with: .automatic)
        tableView.setEditing(!tableView.isEditing, animated: true)
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row % 2 == 0 ? 100 : 50
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let blackView = UIView()
        blackView.backgroundColor = .black
        return blackView
    }

    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        print("TAPPPED")
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("USER CHOOSE CELL \(indexPath)")
        performSegue(withIdentifier: "GoToDetails", sender: nil)
    }

//    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
//        return .insert
//    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "REMOVE"
    }

    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(style: .destructive, title: "REMOVE") { [weak self] _, _, _ in
            self?.removeDataFor(indexPath: indexPath)
        }
        let swipe = UISwipeActionsConfiguration(actions: [action])
        return swipe
    }

    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(style: .normal, title: "COPY") { [weak self] _, _, _ in
//            self?.copyDataFor(indexPath: indexPath)
            self?.performSegue(withIdentifier: "GoToDetails", sender: indexPath)
        }
        action.backgroundColor = .green
        let swipe = UISwipeActionsConfiguration(actions: [action])
        return swipe
    }

    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        let configuration = UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { actions -> UIMenu? in
            let action = UIAction(title: "Custom action", image: nil, identifier: nil) { action in
                print("CUSTOM ACTION TAPPED")
                // Put button handler here
            }
            let actionTwo = UIAction(title: "Custom action 2", image: nil, identifier: nil) { action in
                print("CUSTOM ACTION 2 TAPPED")
                // Put button handler here
            }
            return UIMenu(title: "Menu", image: nil, identifier: nil, options: .displayInline, children: [action, actionTwo])
        }
        return configuration
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return names.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IndexPathCell", for: indexPath)
        // MAIN LABEL
        cell.textLabel?.text = "IndePath.row:\(indexPath.row), Section: \(indexPath.section) \(names[indexPath.row])"
        // SECONDARY LABEL
//        cell.detailTextLabel
//            students[indexPath.row].name // students[indexPath.row].slackName
        return cell
    }

//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 2
//    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Section Number: \(section)"
    }

    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return "Section footer Number: \(section)"
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.row % 2 == 0
    }

    func tableView(_ tableView: UITableView, commit  : UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        switch commit {
        case .delete:
            removeDataFor(indexPath: indexPath)
        case .insert:
            copyDataFor(indexPath: indexPath)
        default: break
        }
//        print(indexPath)
    }

    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let value = names[sourceIndexPath.row]
        names.remove(at: sourceIndexPath.row)
        names.insert(value, at: destinationIndexPath.row)
        print(names)
    }
}
