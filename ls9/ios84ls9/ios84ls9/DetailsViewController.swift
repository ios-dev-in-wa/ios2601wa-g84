//
//  DetailsViewController.swift
//  ios84ls9
//
//  Created by WA on 25.02.2021.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    var userName = String()

    override func viewDidLoad() {
        super.viewDidLoad()

        nameLabel.text = userName
    }

}
