//
//  AddPersonViewController.swift
//  ios84ls15
//
//  Created by WA on 18.03.2021.
//

import UIKit

protocol AddPersonViewControllerDelegate: class {
    func didCreate(person: Person)
}

class AddPersonViewController: UIViewController {

    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var surnameTextfield: UITextField!
    @IBOutlet weak var ageTextfield: UITextField!

    private let manager = CoreDataStack.shared.personManager

    weak var delegate: AddPersonViewControllerDelegate?
    var person: Person?

    override func viewDidLoad() {
        super.viewDidLoad()
        if let person = person {
            nameTextfield.text = person.name
            surnameTextfield.text = person.surname
            ageTextfield.text = String(person.age)
        }
    }
    
    @IBAction func saveAction(_ sender: UIBarButtonItem) {
        guard let name = nameTextfield.text,
              let surname = surnameTextfield.text,
              let age = Int(ageTextfield.text ?? "") else { return }
        if let person = person {
            manager.updatePerson(id: person.uniqueId!, name: name, surname: surname, age: age) { [weak self] person in
                self?.navigationController?.popViewController(animated: true)
                self?.delegate?.didCreate(person: person)
            }
        } else {
            manager.createPerson(name: name, surname: surname, age: age) { [weak self] person in
                self?.navigationController?.popViewController(animated: true)
                self?.delegate?.didCreate(person: person)
            }
        }
    }
}
