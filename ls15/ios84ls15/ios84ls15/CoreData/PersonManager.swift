//
//  PersonManager.swift
//  ios84ls15
//
//  Created by WA on 18.03.2021.
//

import Foundation
import CoreData

class PersonManager {
    private let context: NSManagedObjectContext

    init(context: NSManagedObjectContext) {
        self.context = context
    }

    func createPerson(name: String, surname: String, age: Int, completion: ((Person) -> Void)) {
        guard let entity = NSEntityDescription.entity(forEntityName: "Person", in: context) else { return }

        guard let person = NSManagedObject(entity: entity, insertInto: context) as? Person else { return }
        person.name = name
        person.age = Int16(age)
        person.surname = surname
        person.dateOfRegistration = Date()
        person.uniqueId = UUID().uuidString

        do {
            try context.save()
            completion(person)
        } catch {
            print(error.localizedDescription)
        }
    }

    func updatePerson(id: String, name: String, surname: String, age: Int, completion: ((Person) -> Void)) {
        let fetchRequest = NSFetchRequest<Person>(entityName: "Person")

        fetchRequest.predicate = NSPredicate(format: "uniqueId == %@", id)

        do {
            let persons = try context.fetch(fetchRequest)
            if let person = persons.first {
                person.name = name
                person.surname = surname
                person.age = Int16(age)

                try context.save()
                completion(person)
            }
        } catch {
            print(error.localizedDescription)
        }
    }

    func getPersons(completion: (([Person]) -> Void)) {
        let fetchRequest = NSFetchRequest<Person>(entityName: "Person")
        
        do {
            let persons = try context.fetch(fetchRequest)
            completion(persons)
        } catch {
            print(error.localizedDescription)
        }
    }

    func removePerson(person: Person) {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Person")
        fetchRequest.predicate = NSPredicate(format: "uniqueId == %@", person.uniqueId!)

        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try context.execute(deleteRequest)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    

    func clearAll() {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Person")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try context.execute(deleteRequest)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
}
