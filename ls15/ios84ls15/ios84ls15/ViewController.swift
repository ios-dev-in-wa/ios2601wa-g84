//
//  ViewController.swift
//  ios84ls15
//
//  Created by WA on 18.03.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    let manager = CoreDataStack.shared.personManager
    var persons: [Person] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        let nib = UINib(nibName: "PersonTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "PersonTableViewCell")
        manager.getPersons { [weak self] persons in
            self?.persons = persons
            self?.tableView.reloadData()
        }

//        let date = Date()
//        print(date, date.timeIntervalSince1970)
//
//        let dataFormatter = DateFormatter()
//        print(Locale.availableIdentifiers)
//        let ruLocale = Locale(identifier: "ru")
//        dataFormatter.locale = ruLocale
//        Locale.availableIdentifiers.forEach {
//            print(ruLocale.localizedString(forIdentifier: $0))
//        }
//        dataFormatter.dateFormat = "E -- d -- MMMM -- HH:mm"
//        print(dataFormatter.string(from: date))
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadSections([0], with: .automatic)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "showDetails",
           let destinationVC = segue.destination as? AddPersonViewController {
            destinationVC.delegate = self
            destinationVC.person = sender as? Person
        }
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return persons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PersonTableViewCell", for: indexPath) as? PersonTableViewCell else {
            fatalError()
        }
        let person = persons[indexPath.row]
        cell.textLabel?.text = person.name
        cell.detailTextLabel?.text = person.surname
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "showDetails", sender: persons[indexPath.row])
    }

    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(style: .destructive, title: "Del") { [weak self] _, _, _ in
            guard let self = self else { return }
            self.manager.removePerson(person: self.persons[indexPath.row])
            self.persons.remove(at: indexPath.row)
            self.tableView.reloadSections([0], with: .automatic)
        }
        let configuration = UISwipeActionsConfiguration(actions: [action])
        return configuration
    }
}

extension ViewController: AddPersonViewControllerDelegate {
    func didCreate(person: Person) {
        if let index = persons.firstIndex(where: { $0.uniqueId == person.uniqueId}) {
            persons.remove(at: index)
            persons.insert(person, at: index)
        } else {
            persons.insert(person, at: 0)
        }
    }
}
