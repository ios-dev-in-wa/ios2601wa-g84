//
//  Car.swift
//  ios84ls12
//
//  Created by WA on 11.03.2021.
//

import Foundation
//import Parse

struct Car: Codable {
    let title: String
    let price: Double
    let wasInAccident: Bool
    let objectId: String

    init(title: String, price: Double, wasInAccident: Bool, objectId: String) {
        self.title = title
        self.price = price
        self.wasInAccident = wasInAccident
        self.objectId = objectId
    }

//    init?(parseObject: PFObject) {
//        guard let title = parseObject["title"] as? String,
//              let price  = parseObject["price"] as? Double,
//              let wasInAccident = parseObject["wasInAccident"] as? Bool,
//              let objectId = parseObject.objectId else {
//            return nil
//        }
//        self.title = title
//        self.price = price
//        self.wasInAccident = wasInAccident
//        self.objectId = objectId
//    }
}
