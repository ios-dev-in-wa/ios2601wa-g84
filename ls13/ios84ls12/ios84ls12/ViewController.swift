//
//  ViewController.swift
//  ios84ls12
//
//  Created by WA on 11.03.2021.
//

import UIKit
//import Parse

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    private let apiManager: CarEndpointProtocol = CarManager() //APIManager.shared
    private var cars = [Car]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

//        traitCollection.horizontalSizeClass
//        traitCollection.verticalSizeClass
        tableView.dataSource = self
        tableView.delegate = self
//        apiManager.authorize()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        apiManager.readCars { [weak self] cars in
            self?.cars = cars
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddCarVC", let addVC = segue.destination as? AddCarViewController {
            addVC.car = sender as? Car
        }
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let actionDelete = UIContextualAction(style: .destructive, title: "Delete") { [weak self] _, _, _ in
            guard let self = self else { return }
            let car = self.cars[indexPath.row]
            self.apiManager.deleteCar(car: car)
            self.cars.remove(at: indexPath.row)
            self.tableView.reloadData()
        }
        return UISwipeActionsConfiguration(actions: [actionDelete])
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let car = cars[indexPath.row]
        performSegue(withIdentifier: "AddCarVC", sender: car)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cars.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CarTableViewCell", for: indexPath) as? CarTableViewCell else {
            fatalError()
        }
        cell.setupWith(car: cars[indexPath.row])
        return cell
    }
}
