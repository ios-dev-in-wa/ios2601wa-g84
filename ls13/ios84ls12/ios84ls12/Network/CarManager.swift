//
//  CarManager.swift
//  ios84ls12
//
//  Created by WA on 11.03.2021.
//

import Foundation

struct CarRequest: Codable {
    let title: String
    let price: Double
    let wasInAccident: Bool
}

struct CarCreateResponse: Codable {
    let objectId: String
}

class CarManager: CarEndpointProtocol {

    private let session = URLSession.shared

    func createCar(title: String, price: Double, wasInAccident: Bool, completion: @escaping ((Car) -> Void)) {
        var request = URLRequest(url: URL(string: "https://parseapi.back4app.com/classes/Car")!)
        request.httpMethod = "POST"

        request.addValue("LNx7Tu1Z6JwKlPLp0zQeyfqQdjRrRpYKsHcGpPaK", forHTTPHeaderField: "X-Parse-Application-Id")
        request.addValue("HK0poDd0zMYzqSWjBf0DyPbTauURi5SYb6Pwek3g", forHTTPHeaderField: "X-Parse-REST-API-Key")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        guard let data = try? JSONEncoder().encode(CarRequest(title: title, price: price, wasInAccident: wasInAccident)) else { return }
        request.httpBody = data

        session.dataTask(with: request) { data, response, error in
            if let error = error {
                print("Request error", error.localizedDescription)
                return
            }

            guard let data = data else { return }
            do {
                let carsResponse = try JSONDecoder().decode(CarCreateResponse.self, from: data)
                completion(Car(title: title, price: price, wasInAccident: wasInAccident, objectId: carsResponse.objectId))
            } catch {
                print("DECODING error", error.localizedDescription)
            }
        }.resume()
    }
    
    func readCars(completion: @escaping (([Car]) -> Void)) {
        var request = URLRequest(url: URL(string: "https://parseapi.back4app.com/classes/Car")!)
        request.httpMethod = "GET"
        request.addValue("LNx7Tu1Z6JwKlPLp0zQeyfqQdjRrRpYKsHcGpPaK", forHTTPHeaderField: "X-Parse-Application-Id")
        request.addValue("HK0poDd0zMYzqSWjBf0DyPbTauURi5SYb6Pwek3g", forHTTPHeaderField: "X-Parse-REST-API-Key")

        session.dataTask(with: request) { data, response, error in
            if let error = error {
                print("Request error", error.localizedDescription)
                return
            }

            guard let data = data else { return }
            do {
                let carsResponse = try JSONDecoder().decode(CarResponse.self, from: data)
                completion(carsResponse.results)
            } catch {
                print("DECODING error", error.localizedDescription)
            }
        }.resume()
    }
    
    func deleteCar(car: Car) {
        
    }
    
    func updateCar(car: Car, completion: @escaping ((Car) -> Void)) {
        
    }
}
