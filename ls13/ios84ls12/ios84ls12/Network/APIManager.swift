//
//  APIManager.swift
//  ios84ls12
//
//  Created by WA on 11.03.2021.
//

import Foundation
//import Parse

class APIManager: CarEndpointProtocol {

    static let shared = APIManager()

    private init() { }

    private let session: CarEndpointProtocol = CarManager()//CarParseManager() //CarManager()
    func createCar(title: String, price: Double, wasInAccident: Bool, completion: @escaping ((Car) -> Void)) {
        session.createCar(title: title, price: price, wasInAccident: wasInAccident, completion: completion)
    }
    
    func readCars(completion: @escaping (([Car]) -> Void)) {
        session.readCars(completion: completion)
    }
    
    func deleteCar(car: Car) {
        session.deleteCar(car: car)
    }
    
    func updateCar(car: Car, completion: @escaping ((Car) -> Void)) {
        session.updateCar(car: car, completion: completion)
    }

    func authorize() {
//        PFUser.logInWithUsername(inBackground: "punisher1337", password: "123456") { user, error in
//            if user != nil {
//                print("SUCCESS")
//            }
//        }
    }
    
}
