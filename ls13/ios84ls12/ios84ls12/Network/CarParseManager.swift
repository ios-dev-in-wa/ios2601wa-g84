////
////  CarParseManager.swift
////  ios84ls12
////
////  Created by WA on 11.03.2021.
////
//
//import Foundation
//import Parse
//
//class CarParseManager: CarEndpointProtocol {
//
//    func createCar(title: String, price: Double, wasInAccident: Bool, completion: @escaping ((Car) -> Void)) {
//        let parseObject = PFObject(className:"Car")
//
//        parseObject["title"] = title
//        parseObject["price"] = price
//        parseObject["wasInAccident"] = wasInAccident
//
//        // Saves the new object.
//        parseObject.saveInBackground {
//          (success: Bool, error: Error?) in
//          if success, let car = Car(parseObject: parseObject) {
//            completion(car)
//            print("Success")
//          } else {
//            print("Error saving car")
//          }
//        }
//    }
//    
//    func readCars(completion: @escaping (([Car]) -> Void)) {
//        let query = PFQuery(className:"Car")
//
//        query.findObjectsInBackground { objects, error in
//            if let error = error {
//                print("Error Car read", error.localizedDescription)
//                return
//            }
//            
//            guard let objects = objects else {
//                print("Car Read EMPTY")
//                return
//            }
////            objects.compactMap { object -> Car? in
////                return Car(parseObject: object)
////            }
//            let resultArray = objects.compactMap(Car.init)
//            completion(resultArray)
//        }
//    }
//    
//    func deleteCar(car: Car) {
//        let query = PFQuery(className:"Car")
//
//        query.getObjectInBackground(withId: car.objectId) { object, error in
//            if let error = error {
//                print("Error removing car", error.localizedDescription)
//                return
//            }
//            object?.deleteInBackground()
//        }
//    }
//    
//    func updateCar(car: Car, completion: @escaping ((Car) -> Void)) {
//        let query = PFQuery(className:"Car")
//
//        query.getObjectInBackground(withId: car.objectId) { object, error in
//            if let error = error {
//                print("Error updating car", error.localizedDescription)
//                return
//            }
//            object?["title"] = car.title
//            object?["price"] = car.price
//            object?["wasInAccident"] = car.wasInAccident
//
//            object?.saveInBackground(block: { _, _ in
//                guard let obj = object, let car = Car(parseObject: obj) else { return }
//                completion(car)
//            })
//        }
//    }
//    
//    
//}
