//
//  CarEndpointProtocol.swift
//  ios84ls12
//
//  Created by WA on 11.03.2021.
//

import Foundation

protocol CarEndpointProtocol: class {
    func createCar(title: String, price: Double, wasInAccident: Bool, completion: @escaping ((Car) -> Void))
    func readCars(completion: @escaping (([Car]) -> Void))
    func deleteCar(car: Car)
    func updateCar(car: Car, completion: @escaping ((Car) -> Void))
}
