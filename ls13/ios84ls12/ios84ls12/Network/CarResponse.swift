//
//  CarResponse.swift
//  ios84ls12
//
//  Created by WA on 11.03.2021.
//

import Foundation

struct CarResponse: Codable {
    let results: [Car]
}
