//
//  CarTableViewCell.swift
//  ios84ls12
//
//  Created by WA on 11.03.2021.
//

import UIKit

class CarTableViewCell: UITableViewCell {

    @IBOutlet weak var carTitleLabel: UILabel!
    @IBOutlet weak var carStatusImageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!

    func setupWith(car: Car) {
        carTitleLabel.text = car.title
        carStatusImageView.image = UIImage(systemName: car.wasInAccident ? "car.2" : "car")
        priceLabel.text = "\(car.price) $"
    }
}
