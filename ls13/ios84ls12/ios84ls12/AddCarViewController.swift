//
//  AddCarViewController.swift
//  ios84ls12
//
//  Created by WA on 11.03.2021.
//

import UIKit

//protocol AddCarVCDelegate: class {
//    
//}

class AddCarViewController: UIViewController {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var accidentSwitch: UISwitch!

    private let apiManager = APIManager.shared

    var car: Car?

    override func viewDidLoad() {
        super.viewDidLoad()

        if let car = car {
            titleTextField.text = car.title
            priceTextField.text = "\(car.price)"
            accidentSwitch.isOn = car.wasInAccident
        }
    }

    @IBAction func saveAction(_ sender: UIBarButtonItem) {
        guard let carName = titleTextField.text,
              let carPrice = priceTextField.text,
              let price = Double(carPrice) else { return }
        if let car = car {
            apiManager.updateCar(car: Car(title: carName, price: price, wasInAccident: accidentSwitch.isOn, objectId: car.objectId)) { _ in
                self.navigationController?.popViewController(animated: true)
            }
        } else {
            apiManager.createCar(title: carName, price: price, wasInAccident: accidentSwitch.isOn) { _ in
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        // NAVIGATION

        // MODAL
//        dismiss(animated: true, completion: nil)
    }
}
