//
//  OnboardingViewController.swift
//  ios84l14
//
//  Created by WA on 16.03.2021.
//

import UIKit

class OnboardingViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!

    var model: OnboardingModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        if let model = model {
            imageView.image = UIImage(named: model.imageName)
            titleLabel.text = model.title
            subtitleLabel.text = model.subtitle
            view.backgroundColor = model.backgrounColor
        }
    }
}
