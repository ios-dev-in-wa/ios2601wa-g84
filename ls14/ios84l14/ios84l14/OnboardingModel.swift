//
//  OnboardingModel.swift
//  ios84l14
//
//  Created by WA on 16.03.2021.
//

import UIKit

struct OnboardingModel {
    let imageName: String
    let title: String
    let subtitle: String
    let backgrounColor: UIColor
}
