//
//  PageManagerViewController.swift
//  ios84l14
//
//  Created by WA on 16.03.2021.
//

import UIKit

class PageManagerViewController: UIViewController {

    var pageController: UIPageViewController?
    
    private let onboardingModels: [OnboardingModel] = [
        OnboardingModel(imageName: "onbImageOne", title: "Welcome", subtitle: "You can use this app for ...........................................", backgrounColor: .red),
        OnboardingModel(imageName: "onbImageTwo", title: "Referal system", subtitle: "If you invite your friend to the app you both will get 5$", backgrounColor: .blue),
        OnboardingModel(imageName: "onbImageThree", title: "Bla bla bla", subtitle: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", backgrounColor: .yellow)
    ]

    lazy var viewControllers: [UIViewController] = {
        let controllers: [UIViewController] = onboardingModels.compactMap {
            let viewController = storyboard?.instantiateViewController(withIdentifier: "OnboardingViewController") as? OnboardingViewController
            viewController?.model = $0
            return viewController
        }
        return controllers
    }()
    @IBOutlet weak var progressView: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageController?.dataSource = self
        pageController?.delegate = self
        pageController?.setViewControllers([viewControllers.first!], direction: .forward, animated: true, completion: nil)

        addChild(pageController!)
        view.addSubview(pageController!.view)

        pageController?.didMove(toParent: self)
        view.bringSubviewToFront(progressView)
        progressView.progress = Float(1) / Float(viewControllers.count)
    }
}


extension PageManagerViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard let viewC = pageViewController.viewControllers?.first, let index = viewControllers.firstIndex(of: viewC) else { return }
        if index == 0 {
            progressView.progress = 0
        } else {
            let currentProgress: Float = Float(index + 1) / Float(viewControllers.count)
            progressView.setProgress(currentProgress, animated: true)
        }
    }
}

extension PageManagerViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = viewControllers.firstIndex(of: viewController) else { return nil }
        if index == 0 {
            return nil
        } else {
            return viewControllers[index - 1]
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index = viewControllers.firstIndex(of: viewController) else { return nil }
        if index == viewControllers.count - 1 {
            return nil
        } else {
            return viewControllers[index + 1]
        }
    }
    
    
}
