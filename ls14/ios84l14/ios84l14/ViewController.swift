//
//  ViewController.swift
//  ios84l14
//
//  Created by WA on 16.03.2021.
//

import UIKit
import Photos
import PhotosUI
import SafariServices

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func imageTapAction(_ sender: UITapGestureRecognizer) {
        let alertVC = UIAlertController()
        alertVC.title = "Import image from..."
        alertVC.addAction(UIAlertAction(title: "Camera", style: .default, handler: { [weak self] _ in
            self?.showImagePicker(source: .camera)
        }))
        alertVC.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { [weak self] _ in
            self?.showImagePicker(source: .photoLibrary)
        }))
        present(alertVC, animated: true, completion: nil)
    }

    private func showImagePicker(source: UIImagePickerController.SourceType) {
        let library = PHPhotoLibrary.shared()
        PHPhotoLibrary.requestAuthorization(for: .readWrite) { _ in
            library.presentLimitedLibraryPicker(from: self)
        }
//        let imagePicker = UIImagePickerController()
//        imagePicker.sourceType = source
//        imagePicker.allowsEditing = true
//        imagePicker.delegate = self
//
//        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func showSafaryAction(_ sender: Any) {
        let vc = SFSafariViewController(url: URL(string: "https://apple.com")!)
//        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage ?? info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            return
        }
        imageView.image = image
        print(info)
        picker.dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("User cancelled")
        picker.dismiss(animated: true, completion: nil)
    }
}
