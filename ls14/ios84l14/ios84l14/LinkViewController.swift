//
//  LinkViewController.swift
//  ios84l14
//
//  Created by WA on 16.03.2021.
//

import UIKit
import WebKit
import AVKit

class LinkViewController: UIViewController {

    @IBOutlet weak var webKitView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

//        webKitView.loadHTMLString("""
//            <!DOCTYPE html>
//            <html>
//            <body>
//
//            <h1>My First Heading</h1>
//            <p>My first paragraph.</p>
//
//            </body>
//            </html>
//""", baseURL: nil)
//        webKitView.load(URLRequest(url: URL(string: "https://google.com")!))
//        webKitView.loadFileURL(URL(fileURLWithPath: "/Users/wa/Desktop/videoExample.mov"), allowingReadAccessTo: URL(fileURLWithPath: "/Users/wa/Desktop/videoExample.mov"))
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let avVC = AVPlayerViewController()
        let item = AVPlayerItem(asset: AVAsset(url: URL(fileURLWithPath: "/Users/wa/Desktop/videoExample.mov")))
        avVC.player = AVPlayer(playerItem: item)
        avVC.player?.play()
        
        present(avVC, animated: true, completion: nil)
    }
}
