//
//  UserResponse.swift
//  ios84ls17
//
//  Created by WA on 25.03.2021.
//

import Foundation

struct UserResponse: Codable {
    let results: [User]
}
