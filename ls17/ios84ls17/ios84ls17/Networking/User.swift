//
//  User.swift
//  ios84ls17
//
//  Created by WA on 25.03.2021.
//

import Foundation

struct User: Codable, FullNameUser {
    let gender: String
    let name: Name
//    let location: Location?
    let email: String
    let login: Login
    let dob, registered: Dob
    let phone, cell: String
    let id: ID
    let picture: Picture
    let nat: String

    var fullname: String {
        return "\(name.title) \(name.first) \(name.last)"
    }
}

// MARK: - Dob
struct Dob: Codable {
    let date: String
    let age: Int
}


// MARK: - ID
struct ID: Codable {
    let name: String
    let value: String?
}

// MARK: - Location
struct Location: Codable {
//    let street: Street?
    let city: String?
    let state: String?
    let country: String?
    let postcode: String?
//    let coordinates: Coordinates?
//    let timezone: Timezone
}

// MARK: - Coordinates
struct Coordinates: Codable {
    let latitude, longitude: String
}

// MARK: - Street
struct Street: Codable {
    let number: Int
    let name: String
}


// MARK: - Login
struct Login: Codable {
    let uuid, username, password, salt: String
    let md5, sha1, sha256: String
}

// MARK: - Name
struct Name: Codable {
    let title, first, last: String
}

// MARK: - Picture
struct Picture: Codable {
    let large, medium, thumbnail: String
}
