//
//  UserManager.swift
//  ios84ls17
//
//  Created by WA on 25.03.2021.
//

import Foundation

enum NetworkError: Error {
    case urlIsNotValid
    case noDataReceived
    case responseIsNotValid
    case systemError(Error)
    case statusCodeIsWrong(Int)
}

class UserManager {

    private let session = URLSession.shared

    func getUsers(completion: @escaping ((Result<[User], NetworkError>) -> Void)) {
//        https://randomuser.me/api/?results=10
        guard let url = URL(string: "https://randomuser.me/api/?results=10") else {
            completion(.failure(.urlIsNotValid))
            return
        }
        session.dataTask(with: url) { data, respone, error in
            if let error = error {
                completion(.failure(.systemError(error)))
                return
            }
            guard let resposne = respone as? HTTPURLResponse else {
                completion(.failure(.responseIsNotValid))
                return
            }
            switch resposne.statusCode {
            case 200...201:
                guard let data = data else {
                    completion(.failure(.noDataReceived))
                    return
                }
                do {
                    let dataResponse = try JSONDecoder().decode(UserResponse.self, from: data)
                    completion(.success(dataResponse.results))
                } catch {
                    completion(.failure(.systemError(error)))
                }
            default:
                completion(.failure(.statusCodeIsWrong(resposne.statusCode)))
            }
        }.resume()
    }
}
