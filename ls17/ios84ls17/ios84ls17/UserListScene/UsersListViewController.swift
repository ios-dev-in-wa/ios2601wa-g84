//
//  UsersListViewController.swift
//  ios84ls17
//
//  Created by WA on 25.03.2021.
//

import UIKit

class UsersListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    private lazy var presenter = UsersListPresenter(view: self)

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        presenter.viewDidLoad()
    }

    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: segue)
        if segue.identifier == "toDefails", let vc = segue.destination as? UserProfileViewController {
            vc.presenter.userCreationHandler = { [weak self] user in
                self?.presenter.addUser(user)
            }
        }
    }
}

extension UsersListViewController: UITableViewDelegate {
    
}

extension UsersListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.itemsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath)
        presenter.setupCell(cell, indexPath: indexPath)
        return cell
    }
}

extension UsersListViewController: UsersListPresenterView {
    func refreshTableView() {
        tableView.reloadData()
    }

}
