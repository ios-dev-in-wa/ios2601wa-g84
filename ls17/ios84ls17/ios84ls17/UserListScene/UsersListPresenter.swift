//
//  UsersListPresenter.swift
//  ios84ls17
//
//  Created by WA on 25.03.2021.
//

import UIKit

protocol UsersListPresenterView: class {
    func refreshTableView()
}

class UsersListPresenter {

    private weak var view: UsersListViewController?

    private var users = [FullNameUser]()
    private let userManager = UserManager()

    var itemsCount: Int {
        return users.count
    }

    init(view: UsersListViewController) {
        self.view = view
    }

    func addUser(_ user: FullNameUser) {
        users.insert(user, at: 0)
        view?.refreshTableView()
    }

    func setupCell(_ cell: UITableViewCell, indexPath: IndexPath) {
        cell.textLabel?.text = users[indexPath.row].fullname
    }

    func viewDidLoad() {
        userManager.getUsers { [weak self] result in
            switch result {
            case .failure(let error):
                print(error.localizedDescription)
            case .success(let users):
                self?.users = users
                DispatchQueue.main.async {
                    self?.view?.refreshTableView()
                }
            }
        }
    }
    
}
