//
//  UserProfileViewController.swift
//  ios84ls17
//
//  Created by WA on 25.03.2021.
//

import UIKit

class UserProfileViewController: UIViewController {

    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!

    lazy var presenter = UserProfilePresenter(view: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextFields()
        _ = presenter
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        guard let name = nameTextField.text,
              let surname = surnameTextField.text,
              let age = ageTextField.text else { return }
        presenter.createUser(name: name, surname: surname, age: Int(age) ?? 0)
    }
//
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        view.endEditing(true)
//    }

    private func setupTextFields() {
        [ageTextField, surnameTextField, nameTextField].forEach {
            $0?.delegate = self
        }
    }

    @IBAction func tapGestureAction(_ sender: UITapGestureRecognizer) {
        presenter.userDidTap()
    }
}

extension UserProfileViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case nameTextField:
            surnameTextField.becomeFirstResponder()
        case surnameTextField:
            ageTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return true
    }
}

extension UserProfileViewController: UserProfilePresenterView {
    func keyboardWillAppear(height: CGFloat) {
        guard scrollView.contentOffset == .zero else {
            return
        }
        scrollView.contentOffset = CGPoint(x: 0, y: height)
    }
    
    func keyboardWillHide() {
        scrollView.contentOffset = .zero
    }

    func hideKeyboard() {
        view.endEditing(true)
    }

    func popViewController() {
        navigationController?.popViewController(animated: true)
    }
}
