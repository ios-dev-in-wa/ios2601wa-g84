//
//  UserProfilePresenter.swift
//  ios84ls17
//
//  Created by WA on 25.03.2021.
//

import UIKit

protocol UserProfilePresenterView: class {
    func keyboardWillAppear(height: CGFloat)
    func keyboardWillHide()
    func hideKeyboard()
    func popViewController()
}

protocol FullNameUser {
    var fullname: String { get }
}

struct AppUser: FullNameUser {
    let name: String
    let surname: String
    let age: Int

    var fullname: String { return name + " " + surname }
}

class UserProfilePresenter {

    weak var view: UserProfilePresenterView?

    init(view: UserProfilePresenterView) {
        self.view = view
        setupNotificationCenter()
    }

    var userCreationHandler: ((FullNameUser) -> Void)?

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func userDidTap() {
        view?.hideKeyboard()
    }

    func createUser(name: String, surname: String, age: Int) {
        let user = AppUser(name: name, surname: surname, age: age)
        userCreationHandler?(user)
        view?.popViewController()
    }

    private func setupNotificationCenter() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidAppearHandler), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHideHandler), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    @objc private func keyboardDidAppearHandler(notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            view?.keyboardWillAppear(height: keyboardHeight)
        }
    }
    
    @objc private func keyboardDidHideHandler(notification: Notification) {
        view?.keyboardWillHide()
    }
}
