//
//  ViewController.swift
//  ios84ls17
//
//  Created by WA on 25.03.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var msgLabel: UILabel!
    @IBOutlet weak var tapButton: UIButton!

    private lazy var presenter = Presenter(view: self)

    @IBAction func tapButtonAction(_ sender: UIButton) {
        presenter.buttonAction()
    }
}

extension ViewController: PreseterView {
    func updateView() {
        view.backgroundColor = .yellow
        msgLabel.text = "You are awesome!"
        tapButton.isEnabled = false
    }
}
