//
//  Presenter.swift
//  ios84ls17
//
//  Created by WA on 25.03.2021.
//

import Foundation

protocol PreseterView: class {
    func updateView()
}

class Presenter {

    weak var view: ViewController?
    private var counter = 0

    init(view: ViewController) {
        self.view = view
    }

    func buttonAction() {
        if counter == 5 {
            view?.updateView()
        } else {
            counter += 1
        }
    }
}
