//
//  YellowViewController.swift
//  ios84ls7
//
//  Created by WA on 18.02.2021.
//

import UIKit

protocol Writer {
    func writeAnArticle() -> String
}

class Worker: Writer {
    var name: String = ""

    func writeAnArticle() -> String {
        print("Left hand \(name)")
        return "Masterpiece"
    }
}

protocol YellowViewControllerDelegate: class {
    func userWantToShowGreenVC()
}

class YellowViewController: UIViewController {

    @IBOutlet weak var mainLabel: UILabel!
    var mainLabelText = ""
    let seasonManager = SeasonManager()
    
    
    // ПЛОХО
//    var redVC: RedViewController?

    weak var delegate: YellowViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        mainLabel.text = mainLabelText
        seasonManager.animals.append(Rabbit())
        seasonManager.animals.append(Bear())
        seasonManager.live()
    }

    @IBAction func showGreenVCAction(_ sender: UIButton) {
        delegate?.userWantToShowGreenVC()
        performSegue(withIdentifier: "showGreenVC", sender: nil)
    }
}
