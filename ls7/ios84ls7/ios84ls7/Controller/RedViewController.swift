//
//  RedViewController.swift
//  ios84ls7
//
//  Created by WA on 18.02.2021.
//

import UIKit

class RedViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func showYellowVCAction(_ sender: UIButton) {
        if validationResult() {
            performSegue(withIdentifier: "showYellowVC", sender: nil)
        } else {
            print("Error validation")
        }
    }
    
//    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
//        if identifier == "showYellowVC" {
//            if validationResult() {
//                print("Validation passed")
//                return true
//            } else {
//                return false
//            }
//        }
//    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "noActionSegue":
            guard let yellowVC = segue.destination as? YellowViewController else {
                print("Destination is not Yellow")
                return
            }
            yellowVC.delegate = self
            yellowVC.mainLabelText = "NO ACTION SEGUE TRIGGERED"
        case "showYellowVC":
            //        if let yellowVC = segue.destination as? YellowViewController {
            //            yellowVC.mainLabel
            //        }
            guard let yellowVC = segue.destination as? YellowViewController else {
                print("Destination is not Yellow")
                return
            }
            /*
             // FORCE LOAD VIEW
             _ = yellowVC.view
             yellowVC.mainLabel.text = textField.text
             */
            yellowVC.delegate = self
            yellowVC.mainLabelText = textField.text ?? "DEFAULT TEXT"
            segue.destination.title = "Yellow submarine"
        default: break
        }
    }

    func validationResult() -> Bool {
        return Bool.random()
    }
}

extension RedViewController: YellowViewControllerDelegate {
    func userWantToShowGreenVC() {
        print("SHOW GREEN VC TO USER")
        view.backgroundColor = .blue
    }
}
