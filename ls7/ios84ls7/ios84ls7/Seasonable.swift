//
//  Seasonable.swift
//  ios84ls7
//
//  Created by WA on 18.02.2021.
//

import Foundation

protocol Seasonable {
    func winterIsComming()
    func springIsComming()
    func summerIsComming()
    func autumnIsCommin()
}

enum AttackType {
    case kogti, toothes
}

protocol Predator {
    var attackType: AttackType { get }
}
