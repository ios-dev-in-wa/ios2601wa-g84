//
//  SeasonManager.swift
//  ios84ls7
//
//  Created by WA on 18.02.2021.
//

import Foundation

class SeasonManager {
    var animals: [Seasonable] = []

    func live() {
        for animal in animals {
            animal.winterIsComming()
            animal.springIsComming()
            animal.summerIsComming()
            animal.autumnIsCommin()
        }
    }
}
