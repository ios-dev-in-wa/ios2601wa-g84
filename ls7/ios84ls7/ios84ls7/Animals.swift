//
//  Animals.swift
//  ios84ls7
//
//  Created by WA on 18.02.2021.
//

import UIKit

class Rabbit: Seasonable, Predator {
    var attackType: AttackType = .toothes
    
    var furColor: UIColor = .gray

    func winterIsComming() {
        attackType = .kogti
        print("Rabbit winter")
        furColor = .white
    }
    
    func springIsComming() {
        print("Rabbit spring")
        furColor = .gray
    }
    
    func summerIsComming() {
        print("Rabbit IDLE summer")
    }
    
    func autumnIsCommin() {
        print("Rabbit IDLE autumn")
    }
}

class Bear: Seasonable, Predator {
    var attackType: AttackType = .kogti
    var isInSleepingMode = false
    
    func winterIsComming() {
        print("Bear winter")
        isInSleepingMode = true
    }
    
    func springIsComming() {
        print("Bear spring")
        isInSleepingMode = false
    }
    
    func summerIsComming() {
        print("Bear summer")
        print("LOOKING FOR FOOD")
    }
    
    func autumnIsCommin() {
        print("Bear autumn")
        print("LOOKING FOR FOOD")
    }
}
