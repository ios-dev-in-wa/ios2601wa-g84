//
//  StackViewController.swift
//  ios84ls8
//
//  Created by WA on 23.02.2021.
//

import UIKit

class StackViewController: UIViewController {

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var centeredLabel: UILabel!
    @IBOutlet weak var centerelLabelLeadingCostnraint: NSLayoutConstraint!
    
    @IBAction func buttonAction(_ sender: UIButton) {
//        view.subviews
//        stackView.subviews
//        stackView.arrangedSubviews
        let blackBox = UIView()
        blackBox.backgroundColor = .black
        blackBox.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        stackView.addSubview(blackBox)
//        if stackView.axis == .horizontal {
//            stackView.axis = .vertical
//        } else {
//            stackView.axis = .horizontal
//        }
        stackView.axis = stackView.axis == .horizontal ? .vertical : .horizontal
        stackView.center.x = 20
//        centeredLabel.center.x = 100
        centerelLabelLeadingCostnraint.constant = CGFloat.random(in: 0...200)
    }
}
