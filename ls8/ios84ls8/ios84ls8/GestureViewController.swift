//
//  GestureViewController.swift
//  ios84ls8
//
//  Created by WA on 23.02.2021.
//

import UIKit

class GestureViewController: UIViewController {

    @IBOutlet weak var greenView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func tapGestureAction(_ sender: UITapGestureRecognizer) {
        print("TAPPED", sender.state.rawValue)
        sender.view?.backgroundColor = .random
    }

    var startPos = CGPoint(x: 0, y: 0)
    @IBAction func panGestureAction(_ sender: UIPanGestureRecognizer) {
//        view.frame.maxX
//        CGFloat.random(in: <#T##Range<CGFloat>#>)
//        UIColor.random
        switch sender.state {
        case .began:
            startPos = sender.view?.center ?? .zero
            print(startPos)
        case .changed:
            print(sender.location(in: view), sender.velocity(in: view))
            sender.view?.center = sender.location(in: view)
        case .ended:
            print("Ended, \(startPos)")
            sender.view?.center = startPos
        default: break
        }
//        print(sender.state.rawValue, sender.translation(in: view), sender.velocity(in: view))
    }
    @IBAction func pinchAction(_ sender: UIPinchGestureRecognizer) {
        print(sender.scale, sender.velocity)
        greenView.transform = greenView.transform.scaledBy(x: sender.scale, y: sender.scale)
    }
    
    @IBAction func rotationAction(_ sender: UIRotationGestureRecognizer) {
        print(sender.rotation)
    }

    @IBAction func swipeAction(_ sender: UISwipeGestureRecognizer) {
        print(sender.direction)
    }

    @IBAction func longPressAction(_ sender: UILongPressGestureRecognizer) {
        print("HELLLO THERE", sender.state.rawValue)
    }
}

extension CGFloat {
    static var random: CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random, green: .random, blue: .random, alpha: 1.0)
    }
}
