//
//  AnimationViewController.swift
//  ios84ls12
//
//  Created by WA on 09.03.2021.
//

import UIKit

class AnimationViewController: UIViewController {

    @IBOutlet weak var redView: UIView!

//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        print("TOUCH")
//    }

    @IBAction func firstAnimationAction(_ sender: UIButton) {
        UIView.animate(withDuration: 5) {
            self.redView.frame = CGRect(x: 50, y: 50, width: 100, height: 100)
            self.redView.backgroundColor = .black
            self.redView.layer.cornerRadius = 50
        }
    }

    @IBAction func secondAnimationAction(_ sender: UIButton) {
        UIView.animate(withDuration: 5, delay: 1, options: [.autoreverse, .curveLinear, .repeat]) {
//            self.redView.transform = .init(scaleX: 2, y: 2)
            self.redView.transform = .init(rotationAngle: 2)
        } completion: { isFinished in
            self.redView.transform = .identity
            print("SECOND ANIMATION FINISHED")
        }
    }

    @IBAction func thirdAnimationAction(_ sender: UIButton) {
//        redView.layer.removeAllAnimations()
        UIView.transition(with: redView, duration: 1, options: []) {
            self.redView.backgroundColor = .blue
        } completion: { _ in
            print("TRANSITION FINISHED")
        }

    }
    
}
