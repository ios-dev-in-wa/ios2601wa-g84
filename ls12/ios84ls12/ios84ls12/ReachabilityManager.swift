//
//  ReachabilityManager.swift
//  ios84ls12
//
//  Created by WA on 09.03.2021.
//

import Foundation
import Reachability

class ReachabilityManager {

    static let shared = ReachabilityManager()

    private init() {}

    var reachability: Reachability!

    func start() {
        let reachability = try! Reachability()
        self.reachability = reachability

        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        }
        reachability.whenUnreachable = { _ in
            print("Not reachable")
        }

        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
}
