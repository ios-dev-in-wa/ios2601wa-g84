//
//  HeroTableViewCell.swift
//  ios84ls12
//
//  Created by WA on 09.03.2021.
//

import UIKit

class HeroTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var hairColorLabel: UILabel!
    
}
