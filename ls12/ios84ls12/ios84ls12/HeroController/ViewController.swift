//
//  ViewController.swift
//  ios84ls12
//
//  Created by WA on 09.03.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    private let activityView = UIActivityIndicatorView(style: .large)
    private let heroSession = HeroSession()
    private var heroes = [Hero]()

    override func viewDidLoad() {
        super.viewDidLoad()
        if ReachabilityManager.shared.reachability.connection == .unavailable {
            print("NO INTERNET TODAY, BUDDY")
        }
        tableView.dataSource = self
        setupActivityView()
        heroSession.getHeroes { [weak self] arrayOfHeroes in
            self?.heroes = arrayOfHeroes
            DispatchQueue.main.async {
                self?.activityView.stopAnimating()
                self?.tableView.reloadData()
            }
        }
        
    }

    func setupActivityView() {
        view.addSubview(activityView)
        activityView.center = view.center
        activityView.startAnimating()
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return heroes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HeroTableViewCell", for: indexPath) as? HeroTableViewCell else {
            fatalError()
        }
        let hero = heroes[indexPath.row]
        cell.nameLabel.text = hero.name
        cell.genderLabel.text = hero.gender
        cell.hairColorLabel.text = hero.hairColor
        return cell
    }
    
    
}
