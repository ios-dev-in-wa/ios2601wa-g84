//
//  HeroSession.swift
//  ios84ls12
//
//  Created by WA on 09.03.2021.
//

import Foundation

class HeroSession {

    private let session = URLSession.shared
    // ([Hero]) -> Void
    // REALIZATION
    // { heroes in
    //   print(heroes)
    // }
    func setHeroes(heroes: [Hero]) {
        
    }
    
    // () -> String
    func foo() -> String {
        return ""
    }
    
    
    func getHeroes(completion: @escaping (([Hero]) -> Void)) {
        guard let url = URL(string: "https://swapi.dev/api/people/?page=1&size=10") else {
            print("URL IS NOT VALID")
            return
        }
        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
        request.timeoutInterval = 1.0

        session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let response = response as? HTTPURLResponse else { return }
            switch response.statusCode {
            case 200...201:
                guard let data = data else { return }
//                print(String(data: data, encoding: .utf8))
                do {
                    let heroResponse = try JSONDecoder().decode(HeroResponse.self, from: data)
                    completion(heroResponse.results)
//                    print(heroResponse.results.first?.name)
                } catch {
                    
                }
                print("SUCCESS")
            default: print("ERRROR")
            }
        }.resume()
    }
}
