//
//  PlanetsViewController.swift
//  ios84ls12
//
//  Created by WA on 09.03.2021.
//

import UIKit

class PlanetsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    private let activityView = UIActivityIndicatorView(style: .large)
    private let planetSession = PlanetsSession()
    private var planets = [Planet]()

    override func viewDidLoad() {
        super.viewDidLoad()
        if ReachabilityManager.shared.reachability.connection == .unavailable {
            print("NO INTERNET TODAY, BUDDY")
        }
        tableView.dataSource = self
        setupActivityView()
        planetSession.getPlanets { [weak self] arrayOfPlanets in
            self?.planets = arrayOfPlanets
            DispatchQueue.main.async {
                self?.activityView.stopAnimating()
                self?.tableView.reloadData()
            }
        }
        
    }

    func setupActivityView() {
        view.addSubview(activityView)
        activityView.center = view.center
        activityView.startAnimating()
    }
}

extension PlanetsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return planets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HeroTableViewCell", for: indexPath) as? HeroTableViewCell else {
            fatalError()
        }
        let planet = planets[indexPath.row]
        cell.nameLabel.text = planet.name
        cell.genderLabel.text = planet.climate
        cell.hairColorLabel.text = planet.population
        return cell
    }
    
    
}
