//
//  PlanetResponse.swift
//  ios84ls12
//
//  Created by WA on 09.03.2021.
//

import Foundation

// MARK: - PlanetResponse
struct PlanetResponse: Codable {
    let count: Int
    let next: String
    let previous: String?
    let results: [Planet]
}

// MARK: - Result
struct Planet: Codable {
    let name, rotationPeriod, orbitalPeriod, diameter: String
    let climate, gravity, terrain, surfaceWater: String
    let population: String
    let residents, films: [String]
    let created, edited: String
    let url: String

    enum CodingKeys: String, CodingKey {
        case name
        case rotationPeriod = "rotation_period"
        case orbitalPeriod = "orbital_period"
        case diameter, climate, gravity, terrain
        case surfaceWater = "surface_water"
        case population, residents, films, created, edited, url
    }
}
