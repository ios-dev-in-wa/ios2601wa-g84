//
//  PlanetsSession.swift
//  ios84ls12
//
//  Created by WA on 09.03.2021.
//

import Foundation

class PlanetsSession {

    private let session = URLSession.shared

    func getPlanets(completion: @escaping (([Planet]) -> Void)) {
        guard let url = URL(string: "https://swapi.dev/api/planets/?page=1&size=10") else {
            print("URL IS NOT VALID")
            return
        }
        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
        request.timeoutInterval = 1.0

        session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let response = response as? HTTPURLResponse else { return }
            switch response.statusCode {
            case 200...201:
                guard let data = data else { return }
//                print(String(data: data, encoding: .utf8))
                do {
                    let planetResponse = try JSONDecoder().decode(PlanetResponse.self, from: data)
                    completion(planetResponse.results)
//                    print(heroResponse.results.first?.name)
                } catch {
                    
                }
                print("SUCCESS")
            default: print("ERRROR")
            }
        }.resume()
    }
}
