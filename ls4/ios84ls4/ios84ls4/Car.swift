//
//  Car.swift
//  ios84ls4
//
//  Created by WA on 04.02.2021.
//

import UIKit

enum FuelType: Int {
    case gasoline = 10
    case diesel = 11
    case electricity = 20
    case hydrogen = 666
}

class Car {
    
    var modelName: String// = "Astra"
    var fuelType: FuelType// = "Petrol"
    var color: UIColor// = .black

    init(modelName: String, fuelType: FuelType, color: UIColor) {
        self.modelName = modelName
        self.fuelType = fuelType
        self.color = color
    }

    func startEngine() {
        switch fuelType {
        case .diesel, .gasoline:
            print("Trrr tr")
        case .electricity:
            print("VOID")
        case .hydrogen:
            print("LIQUID DROP")
        } 
    }
}
