//
//  ViewMachine.swift
//  ios84ls4
//
//  Created by WA on 04.02.2021.
//

import UIKit

class ViewMachine {

    func createBox(rect: CGRect, backgroundColor: UIColor) -> UIView {
        let view = UIView()
        view.frame = rect
        view.backgroundColor = backgroundColor
        return view
    }
}
