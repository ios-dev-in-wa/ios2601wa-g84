//
//  ViewController.swift
//  ios84ls4
//
//  Created by WA on 04.02.2021.
//

import UIKit

class ViewController: UIViewController {

    let astra = Car(modelName: "Model 3", fuelType: .electricity, color: .cyan)
    let benz = Car(modelName: "CLE", fuelType: .diesel, color: .brown)

    let viewMachine = ViewMachine()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        // TEST
//        drawTarget(times: 5)
//        let box = UIView()
//        box.backgroundColor = .black
//        box.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
//        box.center = view.center
//        view.addSubview(box)
        //
        
//        Car.init(modelName: <#T##String#>, fuelType: <#T##String#>, color: <#T##UIColor#>)
//        Car(modelName: <#T##String#>, fuelType: <#T##String#>, color: <#T##UIColor#>)
        
//        astra.modelName = "Type 3"
//        FuelType(rawValue: <#T##Int#>)
//        print(astra.fuelType, astra.fuelType.rawValue, astra.modelName)
//        astra.startEngine()
//        benz.startEngine()
        
        // View Machine
        let box = viewMachine.createBox(rect: CGRect(x: 20, y: 20, width: 100, height: 100), backgroundColor: .red)
        view.addSubview(box)
        let boxTwo = viewMachine.createBox(rect: CGRect(x: 100, y: 150, width: 200, height: 10), backgroundColor: .cyan)
        view.addSubview(boxTwo)

        // PASSWORD VALIDATION
//        checkPassword(pass: "_12S")
        // SEARCH
        search(keyword: "da")
    }

    // Homework explanation
    func drawTarget(times: Int) {
//        var xOffset: CGFloat = 10
//        var yOffset: CGFloat = 0
        let padding: CGFloat = 10
        let size: CGFloat = 100
        for i in 1...times {
            let box = UIView()
            box.frame = CGRect(x: (size + padding) * CGFloat(i) , y: 10, width: size, height: size)
//            box.center = CGPoint(x: <#T##CGFloat#>, y: <#T##CGFloat#>)
            box.backgroundColor = .blue
            view.addSubview(box)
            print(astra.modelName, i)
        }
        
    }

//    Задача 1 // Read at home RegEx
//    Проверить пароль на надежность от 1 до 5
//
//    a) если пароль содержит числа +1
//
//    b) символы верхнего регистра +1
//
//    c) символы нижнего регистра +1
//
//    d) спец символы +1
//
//    e) если содержит все вышесказанное
//
//    Пример:
//
//    123456 — 1 a)
//
//    qwertyui — 1 c)
//
//    12345qwerty — 2 a) c)
//
//    32556reWDr — 3 a) b) c)

    func checkPassword(pass: String) {
        var validation: [String] = []

        for char in pass {
            if char.isNumber, !validation.contains("A") {
//                counter += 1 // SHORT HANDS
                validation.append("A")
            }
            if char.isUppercase, !validation.contains("B") {
                validation.append("B")
            }
            if char.isLowercase, !validation.contains("C"){
                validation.append("C")
            }
            if char.isSymbol || char.isMathSymbol || char.isPunctuation, !validation.contains("D") {
                validation.append("D")
            }
        }
        // Bool || Bool - OR ____ bool, bool - AND
        if validation.count == 4  {
            print("password ПРОШЕЛ validation")
        } else {
            print("password isn't valid", validation)
        }
    }

    // Задача 2
//    Сделать выборку из массива строк в которых содержится указанная строка
//
//    [“lada”, “sedan”, “baklazhan”] search “da”

    func search(keyword: String) {
        let array = ["LAda", "sedan", "Dimon", "Kvartira", "KOt", "DANON"]

//        var resultArray = [String]()
//        var resultArray: [String] = []

        // MANUAL
//        for element in array {
//            if element.lowercased().contains(keyword.lowercased()) {
//                resultArray.append(element)
//            }
//        }
        
        // AUTO
        let resultArray = array.filter { element -> Bool in
            return element.lowercased().contains(keyword.lowercased())
        }
        print(resultArray)
    }
}

