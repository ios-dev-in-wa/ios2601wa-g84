//
//  Phone.swift
//  ios84ls4
//
//  Created by WA on 04.02.2021.
//

import Foundation

enum BatteryLevel: Double {
    case low = 10.0
    case medium = 50.0
    case high = 90.0
}

class Phone {
    let batteryLevel: BatteryLevel
    let manufacturer: String

    init(batteryLevel: BatteryLevel, manufacturer: String) {
        self.batteryLevel = batteryLevel
        self.manufacturer = manufacturer
    }

    func phoneDescription() -> String {
        return "\(manufacturer) battery level is: \(batteryLevel.rawValue)"
    }
    
}
