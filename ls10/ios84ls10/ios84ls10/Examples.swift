//
//  Examples.swift
//  ios84ls10
//
//  Created by WA on 02.03.2021.
//

import Foundation

protocol SomeProt {
    func doStuff()
}

class StudentClass: SomeProt {
    var name: String
    var slackName: String
    
    init(name: String, slackName: String) {
        self.name = name
        self.slackName = slackName
    }
    
    func doStuff() {
        
    }
}

struct StudentStruct: SomeProt {
    var name: String
    var slackName: String
    
    func doStuff() {
        
    }
}

struct User {
    let name: String
    let surname: String
    let dateOfBirth: String
}

struct LoginRequest {
    let email: String
    let password: String
}

struct UserResponse {
    let name: String
    let surname: String
    let dateOfBirth: String
    let lastUpdatedTime: Double
    // ...
}
