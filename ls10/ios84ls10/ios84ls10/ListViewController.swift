//
//  ListViewController.swift
//  ios84ls10
//
//  Created by WA on 02.03.2021.
//

import UIKit

struct CellModel: Codable {
    let id: Int
    let placeholder: String
    let switchValue: Bool
    let value: String
}

class ListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageView: UIImageView!
    
    var cellModels = [CellModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
//        saveArray()
//        readArray()

        imageView.image = UIImage(contentsOfFile: "/Users/wa/Desktop/images-2.jpeg")
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
            self.readArray()
        }
        print("123123")
    }

    func saveArray() {
        do {
            let data = try JSONEncoder().encode(cellModels)
            try data.write(to: URL(fileURLWithPath: "/Users/wa/Desktop/sss.txt"))
        } catch {
            print(error.localizedDescription)
        }
    }

//    func saveArray() {
////        let fileName = "text.txt"
//        let value = "Hello there!"
//
//        let documentUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
//        print(documentUrl)
//        guard let data = value.data(using: .utf8) else { return }
//        do {
//            try data.write(to: documentUrl.appendingPathComponent("text.txt"))
//        } catch {
//            print(error.localizedDescription)
//        }
//    }

    func readArray() {
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: "/Users/wa/Desktop/sss33.txt"))
            print(String(data: data, encoding: .utf8))
            let models = try JSONDecoder().decode([CellModel].self, from: data)
            cellModels = models
            tableView.reloadData()
        } catch {
            showError(error: error.localizedDescription)
        }
    }

//    func readFromArray() {
////        let macURL = URL(fileURLWithPath: "/Users/wa/Desktop/sss1.txt")
//        let documentUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
//        do {
//            let data = try Data(contentsOf: documentUrl.appendingPathComponent("text.txt"))
//            let stringValue = String(data: data, encoding: .utf8)
//            print(stringValue)
//        } catch {
//            print(error.localizedDescription)
//        }
//    }

    func showError(error: String) {
        let alertVC = UIAlertController(title: "Error occured!", message: error, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            print("PRINT FROM ALERT ACTION")
//            alertVC.dismiss(animated: true, completion: nil)
        }))
        alertVC.addAction(UIAlertAction(title: "DELETE", style: .destructive, handler: { _ in
            print("DESTRUCTIVE ACTION")
        }))
        present(alertVC, animated: true, completion: nil)
        
//        // Delayed actions
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//            alertVC.dismiss(animated: true, completion: nil)
//        }
    }
}

extension ListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as? TextFieldTableViewCell else {
            fatalError()
        }
        let cellModel = cellModels[indexPath.row]
        cell.setup(cellModel: cellModel)
        cell.delegate = self
        return cell
    }

}

extension ListViewController: TextFieldTableViewCellDelegate {
    func didPressSave(id: Int, value: String) {
        guard let index = cellModels.firstIndex(where: { $0.id == id }) else { return }
//        guard let value = cellModels.first(where: { $0.id == id }) else { return }
        let model = cellModels[index]
        cellModels.remove(at: index)
        cellModels.insert(CellModel(id: model.id, placeholder: model.placeholder, switchValue: model.switchValue, value: value), at: index)
        tableView.reloadSections([0], with: .automatic)
        saveArray()
    }
}
