//
//  ViewController.swift
//  ios84ls10
//
//  Created by WA on 02.03.2021.
//

import UIKit

class ViewController: UIViewController {

//    let arrayOfStudents = [
//        StudentStruct(name: "Artem", slackName: "bazinga"),
//        StudentStruct(name: "Artem", slackName: "bazinga"),
//        StudentStruct(name: "Artem", slackName: "bazinga")
//    ]

    override func viewDidLoad() {
        super.viewDidLoad()
//        var dict = [String: String]()
//        dict["Artem"] = "Bazinga"
//        dict.keys.count
//        print(dict[Array(dict.keys)[0]])

        
        var classStudentOne = StudentClass(name: "Artem", slackName: "bazinga")
        let studentCopy = classStudentOne

        classStudentOne.name = "Ilona"

        print(classStudentOne.name, studentCopy.name)

        var structStudent = StudentStruct(name: "Valera", slackName: "Pupkin")
        let structCopy = structStudent

        structStudent.name = "Anna"

        print("STRUCT")
        print(structStudent.name, structCopy.name)
    }


//    func userLoggedIn(name: String, surname: String, dateOfBirth: String) { }
//    func userLoggedIn(user: User)
}

