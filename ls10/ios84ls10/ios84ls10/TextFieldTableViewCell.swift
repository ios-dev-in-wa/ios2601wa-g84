//
//  TextFieldTableViewCell.swift
//  ios84ls10
//
//  Created by WA on 02.03.2021.
//

import UIKit

protocol TextFieldTableViewCellDelegate: class {
    func didPressSave(id: Int, value: String)
}

class TextFieldTableViewCell: UITableViewCell {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var valueSwitch: UISwitch!

    weak var delegate: TextFieldTableViewCellDelegate?

    var index: Int = 0

    func setup(cellModel: CellModel) {
        textField.placeholder = cellModel.placeholder
        textField.text = cellModel.value
        valueSwitch.isOn = cellModel.switchValue
        textField.isEnabled = valueSwitch.isOn
        index = cellModel.id
    }

    @IBAction func switchValueChange(_ sender: UISwitch) {
        textField.isEnabled = sender.isOn
    }

    @IBAction func saveAction(_ sender: UIButton) {
        delegate?.didPressSave(id: index, value: textField.text ?? "")
    }
}
